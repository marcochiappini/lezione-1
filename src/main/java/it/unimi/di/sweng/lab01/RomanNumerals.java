package it.unimi.di.sweng.lab01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class RomanNumerals {

	public String arabicToRoman(int arab) throws FileNotFoundException {
		if (arab <= 0)
		{
			throw new IllegalArgumentException();
		}
		StringBuilder result = new StringBuilder();
		ArrayList<KeyValuePair> numbers = new ArrayList<KeyValuePair>();
		
		FileReader fileArab = new FileReader("C:\\Users\\Hp\\Software Engeenering\\Lez 1\\lez 1 - v2\\src\\main\\java\\arab.txt");
		Scanner readArab = new Scanner(fileArab);
		
		FileReader fileRoman = new FileReader("C:\\Users\\Hp\\Software Engeenering\\Lez 1\\lez 1 - v2\\src\\main\\java\\roman.txt");
		Scanner readRoman = new Scanner(fileRoman);
		
		readArab.useDelimiter(",");
		readRoman.useDelimiter(",");
		
			while(readArab.hasNextInt() && readRoman.hasNext()){
				numbers.add(new KeyValuePair(readArab.nextInt(), readRoman.next()));
			}

		readArab.close();
		readRoman.close();
		for(KeyValuePair keyValue : numbers){
			arab = letterArabToRoman(arab, result, keyValue.getArab(), keyValue.getRoman());	
		}
		appendI(arab, result);
		
		return result.toString();
	}

	private void appendI(int arab, StringBuilder result) {
		for(int i = 0; i < arab; i++){
			result.append("I");
		}
	}

	private int letterArabToRoman(int arab, StringBuilder result, int numberToCompare, String letterToAppend) {
		while (arab >= numberToCompare){
			result.append(letterToAppend);
			arab -= numberToCompare;
		}
		return arab;
	}

	public int romanToArabic(String romanNumber) throws FileNotFoundException {
		for (int i = 1; i < 4000; i++)
		{
			if (romanNumber.equals(arabicToRoman(i)))
					{
						return i;
					}
		}
		throw new IllegalArgumentException("Invalid Roman numeral");
	}

}
