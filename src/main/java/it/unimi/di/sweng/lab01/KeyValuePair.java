package it.unimi.di.sweng.lab01;

public class KeyValuePair {
	private Integer arab;
	private String roman;
	
	public KeyValuePair(Integer arab, String roman)
	{
		this.arab = arab;
		this.roman = roman;
	}
	
	public Integer getArab() {
		return arab;
	}
	public void setArab(Integer arab) {
		this.arab = arab;
	}
	public String getRoman() {
		return roman;
	}
	public void setRoman(String roman) {
		this.roman = roman;
	}
	
}
